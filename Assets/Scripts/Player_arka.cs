﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_arka : MonoBehaviour
{
    
    public float speed = 5.0f;
    private float positionX;
    private float direction;
    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(0,-5f,0);
    }

    // Update is called once per frame
    void Update()
    {
        direction = Input.GetAxis("Horizontal");

        positionX = transform.position.x + direction*speed*Time.deltaTime;

        transform.position = new Vector3(positionX,transform.position.y,transform.position.z);

        if (transform.position.x>9.65f){
            transform.position = new Vector3(9.55f,transform.position.y,0);
        }else if (transform.position.x<-9.65f){
         transform.position = new Vector3(-9.55f,transform.position.y,0);
        }
        
        
    }
}
