﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class BlockOneLife : BlockArkanoid
{
    protected override void Start(){
        lives=1;
       Colorear();
    }
    protected  void Colorear(){
        SpriteRenderer sp = GetComponent<SpriteRenderer>();
        float r,g,b;
        r = 1f;
        g = 0f;
        b = 0f;
 
        sp.color = new Color(r,g,b,1f);
    }
}